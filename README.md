
Projeto feito em Typscript com Create React App

## Preview

Você pode accesar o projeto online na seguinte URL:

https://metacountries.netlify.app/

## Considerações

- O redux está configurada mas faltou tempo para integrar
- Tudo o que foi feito foi feito em 6h. Por que estava sem tempo devido a empresa que trabalho. Se eu tiver um fim de semana posso terminar tudo.

## Instruções para rodar a aplicação em modo de desenvolvimento

No diretório raiz execute o comando ### `yarn start`
A aplicação estará disponível em localhost na porta 3000.
