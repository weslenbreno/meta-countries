import React from 'react';
import { Card, CardBody, FlagImg, CardTitle, CardBtn, CardHeader } from './styles';

type Props = {
  _id: string;
  img: string;
  name: string;
  capital: string;
};

const CountryCard: React.FC<Props> = ({ img, name, capital, _id }) => {
  return (
    <Card>
      <FlagImg src={img} alt="Flag"></FlagImg>
      <CardBody>
        <CardHeader>
          <CardTitle>{name}</CardTitle>
          <small>{capital}</small>
        </CardHeader>
        <CardBtn to={`/details/${_id}`}>Detalhes</CardBtn>
      </CardBody>
    </Card>
  );
};

export default CountryCard;
