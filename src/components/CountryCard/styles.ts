import styled from 'styled-components';
import {Link} from 'react-router-dom';
export const Card = styled.div`
  display: flex;
  flex-grow: 1;
  background: #ffffff;
  border-radius: 8px;
  box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
  width: 325px;
  min-width: 300px;
  height: 135px;
  display: flex;
  margin: 15px 25px;
`;

export const CardHeader = styled.div`
  text-align: right;
  & small {
    color: rgba(43, 43, 43, 0.8);
    text-align: right;
  }
`;

export const CardBody = styled.div`
  display: flex;
  align-items: flex-end;
  flex-grow: 1;
  border-radius: 8px;
  flex-direction: column;
  padding: 10px 25px;
  justify-content: space-evenly;

`;

export const CardTitle = styled.h2`
  text-align: right;
  color: #2b2b2b;
  font-family: 'VisbyCF Bold', sans-serif;
  padding: 0;
  margin: 0;
  font-size: 18px;
`;

export const FlagImg = styled.img`
  display: block;
  height: 135px;
  object-fit: cover;
  width: 50%;
  border-radius: 8px;
  box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
`;

export const CardBtn = styled(Link)`
  background-color: #2d84ec;
  border: none;
  border-radius: 25px;
  color: #fff;
  padding: 5px 15px;
  box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
  font-size: 14px;
  text-decoration: none;
  font-family: 'VisbyCF DemiBold', sans-serif;
`;