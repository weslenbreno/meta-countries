import React from 'react';
import styled from 'styled-components';

const Search = styled.input`
  border-radius: 25px;
  padding: 15px 25px;
  width: 250px;
  text-align: center;
  border: none;
  background-color: rgba(45, 132, 236, 0.11);
  transition: border 2s;

  &:focus {
    outline: none;
    border: 1px solid#2D84EC;
  }
`;

const SearchBar: React.FC<any> = ({ onSearch }) => {
  return <Search placeholder="Digite o nome do País..." onChange={onSearch} />;
};

export default SearchBar;
