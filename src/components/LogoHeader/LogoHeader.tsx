import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Logo = styled(Link)`
  color: #2d84ec;
  font-size: 22px;
  text-decoration: none;
  font-family: 'VisbyCF Heavy', sans-serif;
`;

const LogoHeader = () => {
  return <Logo to="/">META Countries</Logo>;
};

export default LogoHeader;
