import React, { Suspense } from 'react';
import { ApolloProvider } from '@apollo/client';
import ApolloClient from 'config/apollo';
import { Switch, Route } from 'react-router-dom';
import { Details, Home, EditCountry } from 'routes';
import { Provider } from 'react-redux';
import store from 'store/store';

function App() {
  return (
    <ApolloProvider client={ApolloClient}>
      <Provider store={store}>
        <Suspense fallback={<div></div>}>
          <Switch>
            <Route exact path="/details/:id">
              <Details />
            </Route>
            <Route exact path="/edit/:id">
              <EditCountry />
            </Route>
            <Route exact path="/">
              <Home />
            </Route>
          </Switch>
        </Suspense>
      </Provider>
    </ApolloProvider>
  );
}

export default App;
