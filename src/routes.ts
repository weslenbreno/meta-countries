import React from 'react';

export const Home = React.lazy(() => import('pages/Home/Home'));
export const Details = React.lazy(() => import('pages/Details/Details'));
export const EditCountry = React.lazy(() => import('pages/EditCountry/EditCountry'));


