
import { configureStore, combineReducers, getDefaultMiddleware } from '@reduxjs/toolkit';
import pokedexReducer from './slices/countries';

const rootReducer = combineReducers({
  app: pokedexReducer,
});


const store = configureStore({
  reducer: rootReducer,
  devTools: true,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof rootReducer>;
export default store;