import { createSlice } from '@reduxjs/toolkit';

interface IState {
  countries: any[];
}

const initialState: IState = {
  countries: [],
};

const Favorites = createSlice({
  initialState,
  name: 'countries',
  reducers: {
    editCountry: (state, { payload }) => {
      state.countries = state.countries.map(c => {
          if(c._id === payload._id) {
              return payload;
          }
          return c;
      })
    },
    setCountries: (state, { payload }) => { 
      state.countries = payload;
    },
  },
});

const { actions, reducer } = Favorites;
export const { editCountry, setCountries } = actions;

export default reducer;
