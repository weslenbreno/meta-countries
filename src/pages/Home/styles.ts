import styled from 'styled-components';

export const Container = styled.div`
  flex: 1;
  background-color: #fff;
`;

export const Header = styled.div`
  padding: 25px;
  font-family: 'VisbyCF Heavy';
`;

export const Content = styled.div``;

export const Center = styled.div`
    display: flex;
    justify-content: center;
    margin: 25px;
`;