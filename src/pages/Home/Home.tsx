import { CountryCard, Pagination, SearchBar } from 'components';
import LogoHeader from 'components/LogoHeader/LogoHeader';
import React, { useState, useEffect } from 'react';
import { Content, Header, Center } from './styles';
import { useQuery, gql } from '@apollo/client';
import { Container, Row } from 'react-grid-system';
import { start } from 'repl';

const ALL_COUNTRIES = gql`
  query {
    Country {
      _id
      name
      capital
      flag {
        svgFile
      }
    }
  }
`;

const Home = () => {
  const { loading, error, data } = useQuery(ALL_COUNTRIES);
  const [start, setStart] = useState(1);
  const [end, setEnd] = useState(12);
  const [page, setPage] = useState(0);
  const [countries, setCoutries] = useState([]);

  useEffect(() => {
    setCoutries(data?.Country);
  }, [data]);

  useEffect(() => {
    const begin = page * 12;
    setStart(begin);
    setEnd(begin + 12);
  }, [page]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const onSearch = (event: any) => {
    setPage(0);
    const value = event.target.value || '';
    const result = data?.Country.filter((item: any) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );
    setCoutries(result);
  };

  return (
    <Container>
      <Header>
        <LogoHeader />
      </Header>
      <Center>
        <SearchBar onSearch={onSearch} />
      </Center>
      <Content>
        <Row>
          {countries?.slice(start, end).map((country: any) => (
            <CountryCard
              _id={country._id}
              name={country.name}
              capital={country.capital}
              img={country.flag.svgFile}
              key={country._id}
            />
          ))}
        </Row>
        <Pagination
          count={Math.round(countries?.length / 12)}
          onPageChange={({ selected }) => setPage(selected)}
        />
      </Content>
    </Container>
  );
};

export default Home;
