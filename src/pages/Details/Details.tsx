import React from 'react';
import { Container, Row, Col } from 'react-grid-system';
import { useQuery } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { EditBtn, Header, Item, ItemLabel, ItemText, Title, LogoContainer } from './styles';
import { GET_COUNTRY } from './queries';
import { LogoHeader } from 'components';

const Details = () => {
  let { id } = useParams<any>();
  const { loading, error, data } = useQuery(GET_COUNTRY, {
    variables: { id: id.toString() },
    fetchPolicy: 'network-only',
  });
  let country = data?.Country[0];

  return (
    <Container>
      <LogoContainer>
        <LogoHeader />
      </LogoContainer>
      <Header>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Title>{country?.name}</Title>
          <EditBtn to={`/edit/${country?._id}`}>Editar</EditBtn>
        </div>
      </Header>
      <Row>
        <Col sm={8}>
          <Item>
            <ItemLabel>Capital</ItemLabel>
            <ItemText>{country?.capital}</ItemText>
          </Item>
          <Item>
            <ItemLabel>População</ItemLabel>
            <ItemText>{country?.population}</ItemText>
          </Item>
          <Item>
            <ItemLabel>Área</ItemLabel>
            <ItemText>{`${country?.convertedArea.value} ${country?.convertedArea.unit}`}</ItemText>
          </Item>
          <Item>
            <ItemLabel>Densidade Populacional</ItemLabel>
            <ItemText>{country?.populationDensity}</ItemText>
          </Item>
          <Item>
            <ItemLabel>Idiomas Oficiais</ItemLabel>
            {country?.officialLanguages.map((lang: any) => (
              <ItemText>{lang?.name}</ItemText>
            ))}
          </Item>
          <Item>
            <ItemLabel>Domínios</ItemLabel>
            {country?.topLevelDomains.map((domain: any) => (
              <ItemText>{domain?.name}</ItemText>
            ))}
          </Item>
          <Item>
            <ItemLabel>Código Numérico</ItemLabel>
            <ItemText>{country?.numericCode}</ItemText>
          </Item>
        </Col>
        <Col>
          <img src={country?.flag.svgFile} alt="Flag" width="300" />
        </Col>
      </Row>
    </Container>
  );
};

export default Details;
