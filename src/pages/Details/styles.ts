import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Title = styled.h1`
  font-family: 'VisbyCF Heavy', sans-serif;
  display: inline-block;
`;

export const Header = styled.div`
  display: flex;
  padding: 25px 0px;
`;

export const EditBtn = styled(Link)`
  margin: 0px 25px;
  border: none;
  color: white;
  background-color: #2d84ec;
  border-radius: 25px;
  padding: 8px 25px;
  font-family: 'VisbyCF Bold';
  text-decoration: none;
  font-size: 15px;
  &:focus {
    outline: none;
  }
`;

export const Item = styled.div`
  display: flex;
  flex-direction: column;
  margin: 15px 25px;
`;

export const ItemLabel = styled.h3`
  font-family: 'VisbyCF Heavy';
  color: #2d84ec;
  margin: 0px;
  margin-bottom: 8px;
`;

export const ItemText = styled.span`
  font-family: 'VisbyCF Bold';
  color: #7f95b1;
`;

export const LogoContainer = styled.div`
  padding: 25px 0px;
`;