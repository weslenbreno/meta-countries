import gql from 'graphql-tag';

export const GET_COUNTRY = gql`
  query getCountry($id: String) {
    Country(_id: $id) {
      _id
      name
      capital
      convertedArea {
        value
        unit
      }
      topLevelDomains {
        name
      }
      populationDensity
      numericCode
      officialLanguages {
        name
      }
      population
      flag {
        svgFile
      }
    }
  }
`;