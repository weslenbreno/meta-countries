import { useQuery } from '@apollo/client';
import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { GET_COUNTRY } from './queries';
import { EditBtn, Form, Input, Title } from './styles';

const EditCountry = () => {
  let { id } = useParams<any>();
  const history = useHistory();
  const { loading, error, data } = useQuery(GET_COUNTRY, {
    variables: { id: id.toString() },
    fetchPolicy: 'network-only',
  });
  let country = data?.Country[0];

  return (
    <div>
      <Form action="">
        <Title>Editando {country?.name}</Title>
        <Input type="text" placeholder="Nome" />
        <Input type="text" placeholder="Capital" />
        <Input type="text" placeholder="Área" />
        <Input type="text" placeholder="Código Numérico" />

        <EditBtn onClick={() => history.push(`/details/${country?._id}`)}>Atualizar</EditBtn>
      </Form>
    </div>
  );
};

export default EditCountry;
