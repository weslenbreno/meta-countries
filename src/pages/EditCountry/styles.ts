import styled from 'styled-components';

export const Input = styled.input`
  padding: 15px;
  border-radius: 8px;
  border: none;
  margin-bottom: 25px;
  background-color: rgba(45, 132, 236, 0.11);
`;

export const EditBtn = styled.button`
  margin: 0px 25px;
  border: none;
  color: white;
  background-color: #2d84ec;
  border-radius: 25px;
  padding: 8px 25px;
  font-family: 'VisbyCF Bold';
  text-decoration: none;
  font-size: 15px;
  cursor: pointer;
  width: fit-content;
  &:focus {
    outline: none;
  }

  align-self: flex-end;
`;

export const Title = styled.h1`
  font-size: 25px;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  padding: 50px 20%;
`;